#!/usr/bin/env bash
PROJECTNAME=$1
ENTITY=$2
ENTITYNAME=$3
ENV=$4
SERVICE_EMAIL=$5

GROUPNAME="$ENTITYNAME.$PROJECTNAME.Developers"

if [[ "$ENV" != "dev" ]]
then
    GROUPNAMERUN="$ENTITYNAME.$PROJECTNAME.Run"
    echo "Add service $SERVICE_EMAIL"
   curl -v -X POST \
  $DATABRICKS_HOST/api/2.0/preview/scim/v2/Users \
  -H "Authorization: Bearer $DATABRICKS_TOKEN" \
  -H 'Content-Type: application/javascript' \
  -H 'cache-control: no-cache' \
  -d '{
  "schemas":[
    "urn:ietf:params:scim:schemas:core:2.0:User"
  ],
  "userName":"'$SERVICE_EMAIL'",
  "groups":[],
  "entitlements":[
    {
       "value":"allow-cluster-create"
    }
  ]
}'

    echo "Add service $SERVICE_EMAIL to runners"
    databricks groups add-member --parent-name "$GROUPNAMERUN" --user-name $SERVICE_EMAIL
fi

