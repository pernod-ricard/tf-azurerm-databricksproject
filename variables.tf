variable "keyvault_shared_value" {
  type = string
  default = "shared01"
  description = "Specify the shared name of the keyvault (e.g. shared01)"
}

variable "resourcegroup_shared_value" {
  type = string
  default = "shared01"
  description = "Specify the shared name of the resource group (e.g. shared01)"
}

variable "service_email" {
  type = string
  default = ""
  description = "Specify the email of the service used to run job"
}


variable "azure_devops_project_id" {
  type = string
}


variable "project_name" {
  type = string
}

variable "project_entity" {
  type = string
}

variable "project_application_client_id" {
  type = string
}

/*
variable "azure_tenant_id" {
  type = string
}

variable "azure_subscription_id" {
  type = string
}
*/

/*
variable "azure_devops_project_prefix" {
  type = string
}
*/

variable "azure_devops_project_name" {
  type = string
}

variable "azure_devops_org_service_url" {
  type = string
}

/*
variable "azure_devops_enabled" {
  type = bool
}
*/

variable "azure_devops_pat" {
  type = string
}

variable "databricks_host" {
  type = string
}

variable "databricks_token" {
  type = string
}

variable "environment"{
  type = string
}