terraform {
  required_providers {
    azuredevops = {
      source = "terraform-providers/azuredevops"
    }
    null = {
      source = "hashicorp/null"
    }
  }
  required_version = ">= 0.13"
}
